$(document).ready(function(){
    $("#default").forceNumeric();
    $("#real").forceNumeric({isReal: true});
    $("#no-decimal").forceNumeric({allowDecimal: false});
    $("#no-negative").forceNumeric({allowNegative: false});
    $("#dollar").forceNumeric({isDollar: true});

    var functions = $("#default").data("functions");

    QUnit.module("removeLeadingZeros() tests", function(){
        var removeLeadingZeros = functions.removeLeadingZeros;
        QUnit.test( "leading zeros removed correctly", function( assert ) {
            assert.equal( removeLeadingZeros("00000123456789.00"), "123456789.00", "basic case correctly sanitized, 00000123456789.00 formats to 123456789.00" );
            assert.equal( removeLeadingZeros("-00123"), "-123", "leading minus sign correctly maintained, -00123 formats to -123" );
            assert.equal( removeLeadingZeros("0.05"), "0.05", "decimal 0's ignored, 0.05 formats to 0.05" );
        });
    })

    QUnit.module("isArrowKey() tests", function(){
        var isArrowKey = functions.isArrowKey;
        QUnit.test( "array keys correctly identified", function( assert ) {
            assert.ok(isArrowKey(37) && isArrowKey(38) && isArrowKey(39) && isArrowKey(40), "arrow keys all recognized")
            assert.notOk(isArrowKey(36), "non-arrow keys not recognized")
        });
    })

    QUnit.module("formatNumber() dollar tests", function(){
        var formatNumber = functions.formatNumber;
        var mergeOptions = functions.mergeOptions;
        QUnit.test( "dollar amounts correctly formatted", function( assert ) {
            var options = {
                isDollar: true
            }
            options = mergeOptions(options);
            assert.equal(formatNumber("002224.1235", options), "2224.12", "dollar amount parsed, 002224.1235 formats to 2224.12");
            assert.equal(formatNumber("1", options), "1.00", "dollar amount parsed, 1 formats to 1.00");
            assert.equal(formatNumber("0", options), "0.00", "dollar amount parsed, 0 formats to 0.00");
            assert.equal(formatNumber("1.4", options), "1.40", "dollar amount parsed, 1.4 formats to 1.40");
            assert.equal(formatNumber(".5", options), "0.50", "dollar amount parsed, .5 formats to 0.50");
            assert.equal(formatNumber(".05", options), "0.05", "dollar amount parsed, .05 formats to 0.05");
            assert.equal(formatNumber("-1.4", options), "-1.40", "dollar amount parsed, -1.4 formats to -1.40");
            assert.equal(formatNumber("-.05", options), "-0.05", "dollar amount parsed, -.05 formats to -0.05");
        });
    });

    QUnit.module("isDecimalPressAllowed() tests", function(){
        var isDecimalPressAllowed = functions.isDecimalPressAllowed;
        QUnit.test("ensure that decimal cannot be pressed by configuration", function(assert){
            var options = {
                allowDecimal: false
            }
            var keyEvent = {

            }
            assert.notOk(isDecimalPressAllowed(keyEvent, options), "configuration blocks decimal press");
        })
        QUnit.test("ensure that decimal cannot be pressed if already exists in string", function(assert){
            var options = {
                allowDecimal: true
            }
            var keyEvent = {
                currentValue: "2.50"
            }
            assert.notOk(isDecimalPressAllowed(keyEvent, options), "cannot press decimal if decimal exists");
            var keyEvent = {
                currentValue: "2.50",
                selection: "2."
            }
            assert.ok(isDecimalPressAllowed(keyEvent, options), "can press decimal if decimal exists in selection");
        })
    });

    QUnit.module("isMinusSignPressAllowed() tests", function(){
        var isMinusSignPressAllowed = functions.isMinusSignPressAllowed;
        QUnit.test("ensure that minus sign cannot be pressed by configuration", function(assert){
            var options = {
                allowNegative: false
            }
            var keyEvent = {

            }
            assert.notOk(isMinusSignPressAllowed(keyEvent, options), "configuration blocks minus press");
        })
        QUnit.test("ensure that minus sign cannot be pressed if not at start of string", function(assert){
            var options = {
                allowNegative: true
            }
            var keyEvent = {
                currentValue: "2",
                selectionStart: 1
            }
            assert.notOk(isMinusSignPressAllowed(keyEvent, options), "cannot press minus sign if not at start of string");
            var keyEvent = {
                currentValue: ""
            }
            assert.ok(isMinusSignPressAllowed(keyEvent, options), "can press minus sign if empty string");
            var keyEvent = {
                currentValue: "2.00",
                selectionStart: 0
            }
            assert.ok(isMinusSignPressAllowed(keyEvent, options), "can press minus sign if at start of string");
        })
        QUnit.test("ensure that minus sign cannot be pressed if already exists in string", function(assert){
            var options = {
                allowNegative: true
            }
            var keyEvent = {
                currentValue: "-2.50"
            }
            assert.notOk(isMinusSignPressAllowed(keyEvent, options), "cannot press minus sign if minus exists");
            var keyEvent = {
                currentValue: "-2.50",
                selection: "-2"
            }
            assert.ok(isMinusSignPressAllowed(keyEvent, options), "can press minus sign if minus exists in selection");
        })
    });

});