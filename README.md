# jQuery Numeric #

### Intention ###

Used for enforcing that input on an html text field is numeric input only, as well as enforcing friendly formatting.

### Use ###


```
#!javascript
$("input[type='text']").forceNumeric();
```

### Parameters ###

* **Default (no parameters)**
Enforces that the only allowed input are digits, decimals, and minus signs. Decimals and minus signs cannot be entered as input more than once, and minus signs must be at the start of the string.

* **isReal**
Indicates that the input number should be formatted as a real number. Leading zero's will be removed from any input.

* **isDollar**
Does not allow input beyond a second decimal and also enforces isReal. Performs formatting of the input so that values like ".5" will format to "0.50"

* **allowNegative**
If false, no minus sign is permitted as input.

* **allowDecimal**
If false, no decimal is permitted as input.