(function($){

	var defaultOptions = {
		isReal: false, //removes leading 0's
		isDollar: false, //forces format of ###.##
		allowDecimal: true, //is decimal point allowed
		allowNegative: true //is minus symbol allowed
	}

	//merge options with default options
	var mergeOptions = function(options){
		if(typeof options === "undefined") options = {};
		for(var property in defaultOptions) {
			if (!options.hasOwnProperty(property)) {
				options[property] = defaultOptions[property];
			}
		}
	
		if(options.isDollar){
			options.isReal = true;
		}

		return options;
	}

	var _keycode_to_ascii_map = {
		'188': '44', //,
		'189': '45', //-
		'190': '46', //decimal numrow
		'191': '47', ///
		'192': '96', //`
		'220': '92', //\
		'222': '39', //'
		'221': '93', //]
		'219': '91', //[
		'173': '45', //-

		//numpad
		'96' : '48', //numpad 0
		'97' : '49', //numpad 1
		'98' : '50', //numpad 2
		'99' : '51', //numpad 3
		'100': '52', //numpad 4
		'101': '53', //numpad 5
		'102': '54', //numpad 6
		'103': '55', //numpad 7
		'104': '56', //numpad 8
		'105': '57', //numpad 9
		'109': '45', //minus
		'110': '46', //decimal
		'46' : '127',//delete
		'3'  : '13', //alternative return

		//IE key codes
		'187': '61', //=
		'186': '59', //;
	};
	
	var toAsciiKeyCode = function(c){
		if(_keycode_to_ascii_map.hasOwnProperty(c)){
			return _keycode_to_ascii_map[c];
		}
		return c;
	}
	
	var removeLeadingZeros = function(number){
		number = number.replace(/^0+/, '');
		if(number.indexOf("-") === 0){
			number = "-" + removeLeadingZeros(number.replace("-", ""));
		}else if(number.indexOf(".") === 0){
			number = "0" + number;
		}
		return number;
	}

	var isArrowKey = function(key){
		return key >= 37 && key <= 40;
	}
	
	var formatNumber = function(number, options){
		if(number.length == 1 && (number.indexOf("-") != -1 || number.indexOf(".") != -1)){
			number = "";
		}
		if(number.indexOf("-") == 0){
			var absolute = number.substring(1, number.length);
			absolute = removeLeadingZeros(absolute);
			if(absolute == "0"){
				number = "0";
			}else if(absolute.indexOf(".") == 0){
				number = "-0" + absolute;
			}else{
				number = "-" + absolute;
			}
		}
		if(options.isReal === true && number.length > 1){
			number = removeLeadingZeros(number);
		}
		if(options.isDollar === true){
			if(number.indexOf(".") == -1){
				number = number + ".";
			}
			var parts = number.split(".");
			if(parts[1].length == 0){
				number += "00";
			}else if(parts[1].length == 1){
				number += "0";
			}else if(parts[1].length > 2){
				var decimalPlace = number.indexOf(".");
				number = number.substring(0, decimalPlace + 3);
			}
			
			number = removeLeadingZeros(number);
			
		}
		return number;
	}

	var getCharacterFromAsciiCode = function(ascii){
		return String.fromCharCode(ascii);
	}

	var buildKeyEventObject = function(e, $div){
		var currentValue = $div.val();
		var selectionStart = $div[0].selectionStart;
		var selectionEnd = $div[0].selectionEnd;
		var keyEvent = {
			keyCode: e.keyCode,
			asciiCode: toAsciiKeyCode(e.which || e.keyCode),
			currentValue: currentValue,
			shiftDown: e.shiftKey,
			altDown: e.altKey,
			ctlrDown: e.ctrlKey,
			selectionStart: selectionStart,
			selectionEnd: selectionEnd,
			selection: currentValue.substring(selectionStart, selectionEnd)
		};
		return keyEvent;
	}

	function isCharacterInString(character, string){
		return typeof string === "string" && string.indexOf(character) >= 0;
	}

	var isDecimalPressAllowed = function(keyEvent, options){
		if(!options.allowDecimal || 
			(isCharacterInString('.', keyEvent.currentValue) && 
				!isCharacterInString('.', keyEvent.selection))){
			return false;
		}
		return true;
	}

	var isMinusSignPressAllowed = function(keyEvent, options){
		if(!options.allowNegative) return false;
		if(keyEvent.currentValue === "") return true;
		if((isCharacterInString("-", keyEvent.currentValue) && 
				!isCharacterInString("-", keyEvent.selection))){
			return false;
		}
		if(isCharacterInString("-", keyEvent.selection)) return true;
		if(keyEvent.selectionStart !== 0) return false;
		return true;
	}

	var isValidKeyPress = function(keyEvent, options){
		//allow arrow keys
		if(isArrowKey(keyEvent.asciiCode)){
			if(keyEvent.shiftDown){
				return false;
			}
			return true;
		}

		if(!keyEvent.shiftDown && !keyEvent.altDown && !keyEvent.ctlrDown &&
			// Backspace and Tab and Enter
			keyEvent.asciiCode == 8 || //backspace
			keyEvent.asciiCode == 9 || //tab
			keyEvent.asciiCode == 13 || //enter
			keyEvent.asciiCode == 127){ //delete
			//sure
			return true;
		}

		//digits
		if (keyEvent.asciiCode >= 48 && keyEvent.asciiCode <= 57){
			if(keyEvent.shiftDown){
				return false;
			}
			return true;
		}

		//decimal
		if(keyEvent.asciiCode == 46){
			return isDecimalPressAllowed(keyEvent, options);
		}

		//minus
		if(keyEvent.asciiCode == 45){
			return isMinusSignPressAllowed(keyEvent, options);
		}

		return false;
	}

	var functions = {
		mergeOptions: mergeOptions,
		toAsciiKeyCode: toAsciiKeyCode,
		removeLeadingZeros: removeLeadingZeros,
		isArrowKey: isArrowKey,
		formatNumber: formatNumber,
		isDecimalPressAllowed: isDecimalPressAllowed,
		isMinusSignPressAllowed: isMinusSignPressAllowed,
		isValidKeyPress: isValidKeyPress
	}

	$.fn.forceNumeric = function (options) {
		options = mergeOptions(options);
		return this.each(function () {
			//attaching functions for testing
			$(this).data("functions", functions);
			
			//when key is down, return whether or not that key is a valid press
			$(this).keydown(function (e) {
				var keyEvent = buildKeyEventObject(e, $(this));
				return isValidKeyPress(keyEvent, options)
			});
			
			//when this input looses focus, get value, format, replace, and trigger change
			$(this).blur(function(){
				var number = $(this).val();
				number = formatNumber(number, options);
				$(this).val(number);
				$(this).trigger("change");
			});
		});
	}

})(jQuery);